<?php 
class Classroom extends CI_Controller{


	function create(){

		$data = array(
			'dasaran' => $this->dasModel->find_all('dasaran')
			);


		$this->layout->load('create',$data);

	}
	function save_das(){

		$data = $this->input->post('newDas');

		for($i = 0; $i < count($data); $i++){

			$this->dasModel->Insert('dasaran',array('name'=>$data[$i]));
		}
	}
	function save_ash(){

		$data = $this->input->post('newAsh');

		$cls = $this->input->post('toDas');

		for($i = 0; $i < count($data); $i++){

			$this->dasModel->Insert('usanox',array('name'=>$data[$i],'dasaran_id'=>$cls));
		}

	}
	function classes(){

		$data = array(
			'dasaran' => $this->dasModel->find_all('dasaran')
			);
		$this->layout->load('classes',$data);
	}

	function matyan(){

		$id = $this->uri->segment(3);

		$ararka = $this->uri->segment(4);

		$data = array(

			'usanoxner' => $this->dasModel->find_by('usanox','dasaran_id',$id)
			
		);
		
		$this->layout->load('matyan',$data);
	}


	

}

?>