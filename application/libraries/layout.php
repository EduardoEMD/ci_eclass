<?php

class Layout{

	private $ci;

	function __construct(){

		$this->ci = & get_instance();
	}

	function load($name, $data=array()){

		$this->ci->load->view("layout/header", $data);

		$this->ci->load->view($name, $data);

	}

	
}