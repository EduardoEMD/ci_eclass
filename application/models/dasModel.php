<?php 
 class dasModel extends CI_model{

 	function __construct(){

 		parent::__construct();

 	}

 	function Insert($table,$data){

 		$this->db->insert($table,$data);
 	}

 	function find_all($table,$sort = 'id', $order = 'asc'){

        $this->db->order_by($sort, $order);

        $query = $this->db->get($table);

        return $query->result_array();

    }

    function find_by($table,$by, $param){

        $this->db->where("$by",$param);

        $query = $this->db->get($table);
        
        return $query->result_array();

    }

    function update($table,$id,$data){

    	$this->db->where('id',$id);

    	$this->db->update($table,$data);

    }

    function delete($table,$id){

        if($id != NULL){

            $this->db->where('id', $id);                    

            $this->db->delete($table);                        

        }

    }    


 }