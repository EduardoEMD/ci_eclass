<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#table_cont{
			width: 80%;
			margin: 0 auto;
			margin-top: 5%;
		}
		td,th{
			text-align: center;
			
		}
	</style>
</head>
<body>
	<div id="table_cont">
	<table class="striped">
		<tr>
			<th>id</th>
			<th>Դասարան</th>
			<th>Պատմություն</th>
			<th>Անգլերեն</th>
			<th>Մաթեմատիկա</th>
			<th>Ֆիզիկա</th>
		</tr>
		<?php foreach($dasaran as $cls): ?>
			<?php $id = $cls['id']; ?>
			<tr>
				<td><?= $cls['id']; ?></td>
				<td><?= $cls['name']; ?></td>
				<td><a href=<?= base_url("classroom/matyan/$id/patm"); ?>><button class="btn" >Edit</button></a></td>
				<td><a href=<?= base_url("classroom/matyan/$id/angl"); ?>><button class="btn" >Edit</button></a></td>
				<td><a href=<?= base_url("classroom/matyan/$id/mat"); ?>><button class="btn" >Edit</button></a></td>
				<td><a href=<?= base_url("classroom/matyan/$id/fiz"); ?>><button class="btn" >Edit</button></a></td>
			</tr>
		<?php endforeach;?>
	</table>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>

</html>