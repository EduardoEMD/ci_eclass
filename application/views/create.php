<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css" media="screen">
		#container{
			width: 40%;
			height: auto;
			padding: 3%;
			display: none;
			float: left;
		}
		#create_div{
			width: 98%;
			height: auto;
			margin:0 auto;
			margin-top: 5%;

		}
		#container2{
			padding: 3%;
			display: none;
			width: 40%;
			float: right;
		}
		#save{
			margin-top: 10px;
		}
		select{
			display: inline-block;
			margin-bottom: 30px;
		}

		</style>
</head>
<body>
  

		<button id="add_das" class="btn">Ավելացնել Դասարան</button>
		<button id="add_ash" class="btn">Ավելացնել Ուսանող</button>
		 <div class="input-field col s12">
	<div id="create_div">
		<div id="container">
			
		</div>
		<div id="container2">
			<select>
		      <option value="" disabled selected>Ընտրեք դասարանը</option>
		      <?php foreach ($dasaran as $das): ?>
		      	<option value="<?= $das['id'];?>"><?= $das['name'];?></option>
		      <?php endforeach; ?>
		    </select><br>
		</div>
	</div>
	  <div class="input-field col s12">
    
   
    
 
	
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#add_das').click(function(){
			$('#container').prepend("<input class='clsnm' type='text' placeholder='գրեք նոր դասարանի անվանումը'>")
			$("#container>#send").remove()
			$('#container').show(300)
			$('#container').append('<button id="send" class="btn">Պահպանել Տվյալները</button>')
		})
		$('#container').on("click",'#send',function(){
			var data = []
			$('.clsnm').each(function(){
				if($(this).val() != ""){
					data.push($(this).val())
				}

			})
				$.ajax({
					async:'false',
					type:"post",
					url:"<?= base_url('classroom/save_das') ?>",
					data:{'newDas':data},
					success:function(r){
						alert('Տվյալները պահպանված են')
						location.reload()

					}
				})

		})
		$('#add_ash').click(function(){
			$('#container2').append("<input class='ashnm' type='text' placeholder='գրեք ուսանողի անունը'>")
			$("#container2>#save").remove()
			$('#container2').show(300)
			$('#container2').append('<button id="save" class="btn">Պահպանել Տվյալները</button>')
		})
		$('#container2').on('click','#save',function(){
			var data = []
			var cls = $('#container2 >select').val()
		
			if(cls != null){

			$('.ashnm').each(function(){
				if($(this).val() != ""){
					data.push($(this).val())
				}
			})
			$.ajax({
				async:"false",
				type:'post',
				url:"<?= base_url('classroom/save_ash') ?>",
				data:{'newAsh':data,"toDas":cls},
				success:function(r){
					alert('Տվյալները պահպանված են')
					location.reload()
				}
			})

			}else{
				alert("Ընտրեք դասարան")
			}

		})

	})
</script>

</html>